<?php

class Autoloader
{

    //fonction qui sera appeller pour lancer l'autoloader
    static function register()
    {
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    //fonction qui effectuera le require de toutes les classes necessaires
    static function autoload($class_name)
    {
        $path = 'class/';

        $r_classname = explode('\\', $class_name);
        for ($i = 0; $i < count($r_classname); $i++) {
            if ($r_classname[$i] != 'App') $path .= $r_classname[$i] . ($i === count($r_classname) - 1 ? '.php' : '/');
        }
        require $path;
    }
}
