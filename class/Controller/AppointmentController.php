<?php

namespace Controller;

use Exception;
use Model\Appointment;
use Model\Patient;
use RouterException;

class AppointmentController
{

    public function add()
    {
        $currentDate = date('Y-m-d\TH:i');
        $listePatients = Patient::getAllPatients();
        require('class/View/appointments/ajout-rendezvous.php');
        exit;
    }

    public function create()
    {
        $dateHour = (isset($_POST['dateHour']) && !empty($_POST['dateHour'])) ? $_POST['dateHour'] : '';
        $idPatient = (isset($_POST['idPatient']) && !empty($_POST['idPatient'])) ? intval($_POST['idPatient']) : '';
        if (Patient::patientExist($idPatient)) {
            $rdv = new Appointment($dateHour, $idPatient);
            $rdv->id = null;
            $rdv->saveAppointment();
            if ($rdv) {
                $this->show($rdv->id);
                exit;
            }
            throw new Exception('Erreur Lors de la sauvegarde du rendez-vous');
        }
        $this->showAll();
        exit;
    }

    public static function showAll()
    {
        $listeRdv = Appointment::getAllAppointments();
        require('class/View/appointments/liste-appointments.php');
        exit;
    }

    public static function show(int $id)
    {
        if ($appointment = Appointment::getAppointmentByID($id)) {
            $patient = Patient::getPatientByID($appointment->idPatients);
            require('class/View/appointments/rendezvous.php');
            return true;
        }

        AppointmentController::showAll();
        return false;
    }

    public function loadEdit($id)
    {
        if ($appointment = Appointment::getAppointmentByID($id)) {
            $listePatients = Patient::getAllPatients();
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $appointment->dateHour);
            $parsedDate = date('Y-m-d\TH:i:s', $date->getTimestamp());
            $currentDate = date('Y-m-d\TH:i');
            require('class/View/appointments/edit-rendezvous.php');
            exit;
        }
        throw new Exception("Erreure, il n'existe pas de rendez-vous ID : $id");
    }

    public function delete($id)
    {
        $appointment = Appointment::getAppointmentByID($id);
        if ($appointment->deleteAppointment()) {
            $this->ShowAll();
            exit;
        }
        throw new Exception("Erreure lors de la suppression du rendez-vous $id");
    }

    public function edit(int $id)
    {
        $appointment = Appointment::getAppointmentByID($id);
        $dateHour = (isset($_POST['dateHour']) && !empty($_POST['dateHour'])) ? $_POST['dateHour'] : '';
        $idPatient = (isset($_POST['idPatient']) && !empty($_POST['idPatient'])) ? intval($_POST['idPatient']) : '';

        if (Patient::patientExist($idPatient)) {
            $appointment->dateHour = $dateHour;
            $appointment->idPatients = $idPatient;
            if ($appointment->saveAppointment()) {
                $this->Show($appointment->id);
            } else {
                throw new Exception('Erreur lors de la sauvegarde');
            }
        }
    }
}
