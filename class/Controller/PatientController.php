<?php

namespace Controller;

use Exception;
use Model\Patient;
use Model\Appointment;
use Model\Database;

class PatientController
{

    public function Add()
    {
        require('class/View/patient/ajout-patient.php');
    }

    public function create()
    {
        $lastname = (isset($_POST['lastname']) && !empty($_POST['lastname']) ? $_POST['lastname'] : '');
        $firstname = (isset($_POST['firstname']) && !empty($_POST['firstname']) ? $_POST['firstname'] : '');
        $birthdate = (isset($_POST['birthdate']) && !empty($_POST['birthdate']) ? $_POST['birthdate'] : '');
        $phone = (isset($_POST['phone']) && !empty($_POST['phone']) ? $_POST['phone'] : '');
        $mail = (isset($_POST['mail']) && !empty($_POST['mail']) ? $_POST['mail'] : '');

        //  && filter_var($birthdate, FILTER_VALIDATE_EMAIL)
        if ($birthdate < date('Y-m-d')) {
            $patient = new Patient($lastname, $firstname, $birthdate, $phone, $mail);
            if ($patient->savePatient()) {
                $this->show($patient->id);
                return true;
            }
            throw new Exception('Erreur lors de la sauvegarde du patient');
        }

        $this->showAll();
        return false;
    }

    public function show($id)
    {
        if ($patient = Patient::getPatientByID($id)) {
            $listeAppointments = Appointment::getAppointmentsForPatientID($patient->id);
            require('class/View/patient/affiche-patient.php');
            return true;
        }

        $this->showAll();
        return false;
    }

    public static function showAll(?int $page = 1)
    {

        $recherche = (isset($_REQUEST['recherche']) && !empty($_REQUEST['recherche']) ? $_REQUEST['recherche'] : null);
        // if ($recherche) $listePatients = Patient::searchPatient($recherche, $page);
        // else $listePatients = Patient::getPatientsPagination($page);
        $wh = [];
        $whStr = '';
        $offset = ($page - 1) * 10;
        if ($recherche) {
            $wh[] = "firstname LIKE '%$recherche%'";
            $wh[] = "lastname LIKE '%$recherche%'";
            $whStr = "WHERE " . implode(' OR ', $wh) . ' ';
        }
        $query = "SELECT * FROM patients " . $whStr . "LIMIT $offset,10";
        $queryCount = "SELECT COUNT(*) AS c FROM patients " . $whStr;
        $result = Database::getConnection()->query($query);
        $statementCount = Database::getConnection()->query($queryCount);
        $resultCount = $statementCount->fetch();

        $listePatients = [];
        foreach ($result->fetchAll() as $patient) {
            $listePatients[] = Patient::instanciateFromDB($patient);
        }
        $maxPages = ($resultCount['c'] >= 10) ? ceil($resultCount['c'] / 10) : 1;

        require('class/View/patient/liste-patients.php');
        exit;
    }

    public function loadEdit($id)
    {
        if ($patient = Patient::getPatientByID($id)) {
            require('class/View/patient/edit-patient.php');
            exit;
        }
        throw new Exception("Patient Introuvable $id");
    }

    public function edit($id)
    {
        $lastname = (isset($_POST['lastname']) && !empty($_POST['lastname']) ? $_POST['lastname'] : '');
        $firstname = (isset($_POST['firstname']) && !empty($_POST['firstname']) ? $_POST['firstname'] : '');
        $birthdate = (isset($_POST['birthdate']) && !empty($_POST['birthdate']) ? $_POST['birthdate'] : '');
        $phone = (isset($_POST['phone']) && !empty($_POST['phone']) ? $_POST['phone'] : '');
        $mail = (isset($_POST['mail']) && !empty($_POST['mail']) ? $_POST['mail'] : '');

        if ($birthdate < date('Y-m-d')) {
            if ($patient = Patient::getPatientByID($id)) {
                $patient->lastname = $lastname;
                $patient->firstname = $firstname;
                $patient->birthdate = $birthdate;
                $patient->phone = $phone;
                $patient->mail = $mail;
                $patient->savePatient();
                $this->show($patient->id);
                exit;
            }
            throw new Exception("ID $id introuvable");
        }
        throw new Exception('Erreur : données fournies lors de l\'edit invalides');
    }

    public function delete($id)
    {
        $patient = Patient::getPatientByID($id);
        $deletePatient = $patient->deletePatient();

        if ($deletePatient) {
            $this->showAll();
            exit;
        }
        throw new Exception("Erreur lors de la supression du patient $id");
    }

    public function loadPlusAppointment()
    {
        require('class/View/patient/ajoute-patient-appointment.php');
        exit;
    }

    public function addPlusAppointment()
    {
        $lastname = (isset($_POST['lastname']) && !empty($_POST['lastname']) ? $_POST['lastname'] : '');
        $firstname = (isset($_POST['firstname']) && !empty($_POST['firstname']) ? $_POST['firstname'] : '');
        $birthdate = (isset($_POST['birthdate']) && !empty($_POST['birthdate']) ? $_POST['birthdate'] : '');
        $phone = (isset($_POST['phone']) && !empty($_POST['phone']) ? $_POST['phone'] : '');
        $mail = (isset($_POST['mail']) && !empty($_POST['mail']) ? $_POST['mail'] : '');

        $dateHour = (isset($_POST['dateHour']) && !empty($_POST['dateHour'])) ? $_POST['dateHour'] : '';

        $patient = new Patient($lastname, $firstname, $birthdate, $phone, $mail);
        if ($patient->savePatient()) {
            $appointment = new Appointment($dateHour, $patient->id);
            if ($appointment->saveAppointment()) {
                $this->show($patient->id);
                return true;
            }
            throw new Exception("Erreur lors de la creation du rendez vous a l'horaire $dateHour");
        }
        throw new Exception("Erreur lors de la creation du Patient");
    }
}
