<?php

namespace Model;

use Model\Patient;

class Appointment
{
    public ?int $id = null;
    public string $dateHour;
    public int $idPatients;

    public function __construct($dateHour = '', $idPatients = '')
    {
        $this->dateHour = $dateHour;
        $this->idPatients = $idPatients;
    }

    public function getPatient(): ?Patient
    {
        return Patient::getPatientByID($this->idPatients);
    }

    public function saveAppointment(): bool
    {
        if ($this->id) {
            $query = "UPDATE appointments SET datehour = ?,idpatients = ? WHERE id = ?";
            $params = [$this->dateHour, $this->idPatients, $this->id];
        } else {
            $query = "INSERT INTO appointments(dateHour, idPatients) VALUES (?,?)";
            $params = [$this->dateHour, $this->idPatients];
        }
        $db = Database::getConnection();
        $statement = $db->prepare($query);
        if ($statement->execute($params)) {
            if (!$this->id) {
                $this->id  = $db->lastInsertId();
            }
            return true;
        }
        return false;
    }

    public static function getAllAppointments(): array
    {
        $stack = [];
        $query = "SELECT * FROM appointments";
        $statement = Database::getConnection()->query($query);
        $rdvs = $statement->fetchAll();
        foreach ($rdvs as $rdv) {
            $stack[] = Appointment::instanciateFromDB($rdv);
        }
        return $stack;
    }

    public static function getLastAppointment(): ?Appointment
    {
        $query = "SELECT * FROM appointments ORDER BY ID DESC LIMIT 1";
        $statement = Database::getConnection()->query($query);
        if ($result = $statement->fetch()) {
            $rdvID = new Appointment($result['dateHour'], $result['idPatients']);
            $rdvID->id = $result['id'];
            return $rdvID;
        };
        return null;
    }

    private static function instanciateFromDB(array $result): Appointment
    {
        $appointment = new Appointment($result['dateHour'], $result['idPatients']);
        $appointment->id = $result['id'];
        return $appointment;
    }

    public static function getAppointmentByID(int $id): ?Appointment
    {
        $query = "SELECT * FROM appointments WHERE id=?";
        $statement = Database::getConnection()->prepare($query);
        $statement->execute([$id]);
        if ($result = $statement->fetch()) {
            return Appointment::instanciateFromDB($result);
        }
        return null;
    }

    public static function getAppointmentsForPatientID(int $idPatient): array
    {
        $stack = [];
        $query = "SELECT * FROM appointments WHERE idPatients = ? ORDER BY dateHour";
        $statement = Database::getConnection()->prepare($query);
        $statement->execute([$idPatient]);
        $rdvs = $statement->fetchAll();
        foreach ($rdvs as $rdv) {
            $stack[] = Appointment::instanciateFromDB($rdv);
        }
        return $stack;
    }

    public function deleteAppointment(): bool
    {
        $query = "DELETE FROM appointments WHERE id=?";
        $statement = Database::getConnection()->prepare($query);
        return $statement->execute([$this->id]);
    }

    public static function deleteAppointmentsByPatientID(Patient $patient): bool
    {
        $query = "DELETE FROM appointments WHERE idPatients=?";
        $statement = Database::getConnection()->prepare($query);
        return $statement->execute([$patient->id]);
    }
}
