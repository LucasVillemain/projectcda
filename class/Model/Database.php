<?php

namespace Model;

use PDO;

class Database
{
    private string $host = 'localhost';
    private string $user = 'root';
    private ?string $pwd = '';
    private string $dbname = 'hospitale2n';
    private static ?PDO $db = null;

    protected function connect()
    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        $pdo = new PDO($dsn, $this->user, $this->pwd);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }


    static function getConnection(): PDO
    {
        if (self::$db) return self::$db;

        $tmp = new Database();
        self::$db = $tmp->connect();
        return self::$db;
    }
}
