<?php

namespace Model;

use Model\Appointment;

class Patient
{

    public ?int $id = null;
    public string $lastname;
    public string $firstname;
    public string $birthdate;
    public ?string $phone;
    public string $mail;

    public function __construct($lastname = '', $firstname = '', $birthdate = '', $phone = '', $mail = '')
    {

        $this->lastname = $lastname;
        $this->firstname = $firstname;
        $this->birthdate = $birthdate;
        $this->phone = $phone;
        $this->mail = $mail;
    }

    public static function getPatientByID(int $id): ?Patient
    {
        $query = "SELECT * FROM patients WHERE id=?";
        $statement = Database::getConnection()->prepare($query);
        $statement->execute([$id]);
        if ($result = $statement->fetch()) {
            return Patient::instanciateFromDB($result);
        }
        return null;
    }

    public static function instanciateFromDB(array $result): Patient
    {
        $patient = new Patient($result['lastname'], $result['firstname'], $result['birthdate'], $result['phone'], $result['mail']);
        $patient->id = $result['id'];
        return $patient;
    }

    /**
     * @return Patient[]
     */
    public static function getAllPatients(): array
    {
        $stack = [];
        $query = "SELECT * FROM patients";
        $statement = Database::getConnection()->query($query);
        $patients = $statement->fetchAll();
        foreach ($patients as $data) {
            $stack[] = Patient::instanciateFromDB($data);
        }
        return $stack;
    }

    public static function getPatientsPagination(int $numPage): array
    {
        $offset = ($numPage - 1) * 10; //offset de 10 elements par page

        $stack = [];
        $query = "SELECT * FROM patients LIMIT $offset,10";
        $statement = Database::getConnection()->query($query);

        $patients = $statement->fetchAll();
        foreach ($patients as $data) {
            $stack[] = Patient::instanciateFromDB($data);
        }
        return $stack;
    }

    public function savePatient(): bool
    {
        if ($this->id) {
            $query = "UPDATE patients SET lastname =?, firstname =?, birthdate = ?, phone = ?, mail = ? WHERE id=? ";
            $params = [$this->lastname, $this->firstname, $this->birthdate, $this->phone, $this->mail, $this->id];
        } else {
            $query = "INSERT INTO patients(lastname, firstname, birthdate, phone, mail) VALUES (?,?,?,?,?)";
            $params = [$this->lastname, $this->firstname, $this->birthdate, $this->phone, $this->mail];
        }
        $db = Database::getConnection();
        $statement = $db->prepare($query);
        if ($statement->execute($params)) {
            if (!$this->id) {
                $this->id  = $db->lastInsertId();
            }
            return true;
        }
        return false;
    }

    public static function getLastPatient(): ?Patient
    {
        $query = "SELECT * FROM patients ORDER BY ID DESC LIMIT 1";
        $statement = Database::getConnection()->query($query);
        if ($result = $statement->fetch()) {
            $patient = new Patient($result['lastname'], $result['firstname'], $result['birthdate'], $result['phone'], $result['mail'], $result['id']);
            return $patient;
        };
        return null;
    }



    public function deletePatient(): bool
    {
        $deletedrdv = Appointment::deleteAppointmentsByPatientID($this);
        $query = "DELETE FROM patients WHERE id = ?";
        $statement = Database::getConnection()->prepare($query);
        return $statement->execute([$this->id]);
    }

    public static function searchPatient(?string $key, ?int $numPage = 1): ?array
    {
        if ($key) {
            $stack = [];
            $offset = ($numPage - 1) * 10;
            $query = "SELECT * FROM patients WHERE firstname LIKE '%$key%' OR lastname LIKE '%$key%' LIMIT $offset,10";
            $statement = Database::getConnection()->query($query);

            $resultat = $statement->fetchAll();
            foreach ($resultat as $patient) {
                $stack[] = Patient::instanciateFromDB($patient);
            }
            return $stack;
        }
        return null;
    }

    public static function patientExist(?int $idPatient): bool
    {
        $query = "SELECT * FROM patients where id = ?";
        $statement = Database::getConnection()->prepare($query);
        $statement->execute([$idPatient]);
        if ($result = $statement->fetch()) {
            return true;
        }
        return false;
    }

    public static function nombrePatient(): int
    {
        $query = "SELECT COUNT(*) FROM patients";
        $statement = Database::getConnection()->query($query);
        if ($result = $statement->fetch()) {
            return $result['COUNT(*)'];
        }
        return 0;
    }
}
