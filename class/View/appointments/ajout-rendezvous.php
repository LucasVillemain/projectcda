<?php
require_once('class/View/header.php');

/**
 * @var $currentDate
 */
?>
<div class="container">
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <form action="new" method="post">
                <div class="mb-3">
                    <label for="DateHeure" class="form-label">Heure du rendez-vous :</label>
                    <input name="dateHour" type="datetime-local" class="form-control" id="DateHour" required min="<?= $currentDate ?>">
                </div>
                <div class="mb-3">
                    <label for="Patient" class="form-label">Patient :</label>
                    <select name="idPatient" id="idPatient">
                        <?php
                        foreach ($listePatients as $patient) {
                        ?>
                            <option value="<?= $patient->id ?>"> <?= $patient->firstname . ' ' . $patient->lastname ?></option>

                        <?php
                        }
                        ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col">

        </div>
    </div>
</div>



<?php
require_once('class/View/footer.php');
