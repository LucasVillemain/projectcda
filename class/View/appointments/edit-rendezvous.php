<?php
require_once('class/View/header.php');

/**
 * @var Appointment $appointment
 * @var Patient $patient
 */

?>

<div class="container">
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <form action="<?= $appointment->id ?>" method="post">
                <div class=" mb-3">
                    <label for="dateHour" class="form-label">Date du rendez-vous :</label>
                    <input name="dateHour" type="datetime-local" class="form-control" id="dateHour" aria-describedby="lastnameHelp" value="<?= $parsedDate ?>" min="<?= $currentDate ?>">
                </div>
                <div class=" mb-3">
                    <label for="firstname" class="form-label">Prenom</label>
                    <select name="idPatient" id="idPatient">
                        <?php

                        foreach ($listePatients as $patient) {
                        ?>
                            <option value="<?= $patient->id ?>" <?= $patient->id === $appointment->idPatients ? 'selected' : '' ?>> <?= $patient->firstname . ' ' . $patient->lastname ?></option>

                        <?php
                        }
                        ?>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Valider</button>
            </form>
        </div>
        <div class="col">

        </div>
    </div>
</div>



<?php
require_once('class/View/footer.php');
