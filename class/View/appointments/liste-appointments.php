<?php
require_once('class/View/header.php');

/**
 * @var listeRdv
 */


?>

<div class="container">
    <div>
        <a href="<?= Route::getBaseURL() . 'rdv/new' ?>" class="btn btn-primary">Creer un nouveau rendez-vous</a>
    </div>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Date du rendez-vous</th>
                <th scope="col">Patient</th>
                <th scope="col">Supprimer</th>
            </tr>
        </thead>
        <tbody>

            <!-- Partie repetee pour chaque rendez vous -->
            <?php
            foreach ($listeRdv as $rdv) {
                $patient = $rdv->getPatient();
            ?>
                <tr ondblclick="window.location='<?= $rdv->id ?>'">

                    <th scope="row"><?= $rdv->id ?></th>
                    <td><?= $rdv->dateHour ?></td>
                    <td><?= $patient ? "$patient->firstname $patient->lastname" : '' ?></td>
                    <td><a href="<?= Route::getBaseURL() . 'rdv/delete/' . $rdv->id ?>" class="btn btn-secondary">Supprimer</a> </td>



                </tr>

            <?php } ?>


        </tbody>
    </table>
</div>



<?php
require_once('class/View/footer.php');
