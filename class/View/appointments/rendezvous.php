<?php
require_once('class/View/header.php');

$isactive = ($_SERVER['REQUEST_URI'] === '/projectCDA/patient/10' ? 'active' : '');

/**
 * @var Appointment $appointment
 */

?>


<div class="container">
    <div class="row">
        <div class="col">
            <h3> Informations sur le rendez-vous </h3>
            <label> Date et heure : </label>
            <p><?= $appointment->dateHour ?></p>
            <label> Patient : </label>
            <p><?= $patient->firstname . ' ' . $patient->lastname . ' (né le ' . $patient->birthdate . ')' ?></p>
            <a href="<?= Route::getBaseURL() . 'rdv/edit/' . $appointment->id ?>" class="btn btn-warning">Editer le rendez-vous</a>
        </div>
    </div>
</div>



<?php
require_once('class/View/footer.php');
