<?php

$urlPatient = Route::getBaseURL() . 'patient/All';
$urlRdv = Route::getBaseURL() . 'rdv/All';
$urlRdvNouveauPatient = Route::getBaseURL() . 'patient/new/appointment';
$currentURL = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ProjectCDA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="http://<?= $_SERVER['HTTP_HOST'] ?>/projectCDA/style.css" rel="stylesheet">
</head>

<body>

    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="<?= $urlPatient ?>">
                    ProjectCDA
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link <?= $currentURL === $urlPatient ? 'active' : '' ?>" href="<?= $urlPatient ?>">Patients</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?= $currentURL === $urlRdv ? 'active' : '' ?>" href="<?= $urlRdv ?>">Rendez-vous</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?= $currentURL === $urlRdvNouveauPatient ? 'active' : '' ?>" href="<?= $urlRdvNouveauPatient ?>">Rendez-vous nouveau patient</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>