<?php
require_once('class/View/header.php');

/**
 * @var Patient $patient
 * @var $listeAppointments
 */

?>

<div class="container">
    <div class="row" style="margin-top:50px">
        <div class="col">
            <h3> Informations sur le patient </h3>
            <label> Nom :</label>
            <p><?= $patient->lastname ?></p>
            <label> prenom :</label>
            <p><?= $patient->firstname ?></p>
            <label> date de naissance :</label>
            <p><?= $patient->birthdate ?></p>
            <label> Numero de telephone :</label>
            <p><?= $patient->phone ?></p>
            <label> email :</label>
            <p><?= $patient->mail ?></p>

            <a href="<?= Route::getBaseURL() . 'patient/edit/' . $patient->id ?>" class="btn btn-warning">Editer le patient</a>
        </div>
        <div class="col-8">
            <h3 class="center"> <?= $listeAppointments ? 'Liste des rendez-vous du patient: ' : ' Le patient n\'a pas de rendez vous pour l\'instant ' ?></h3>

            <div class="container container-flex">
                <!-- Ici repetition de la flexbox pour chaque rendez vous du patient -->
                <?php
                if ($listeAppointments) {
                    foreach ($listeAppointments as $rdv) {
                ?>
                        <a href="<?= Route::getBaseURL() . 'rdv/' . $rdv->id ?>" class="btn flex-item">
                            <div>
                                <label>Date du rendez-vous</label>
                                <p><?= date("d-m-Y H:s", strtotime($rdv->dateHour)) ?></p>
                            </div>
                        </a>

                <?php
                    }
                }
                ?>
            </div>


        </div>
    </div>
</div>



<?php
require_once('class/View/footer.php');
