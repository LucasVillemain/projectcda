<?php
require_once('class/View/header.php');
?>


<div class="container">
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <form action="new" method="post">
                <div class="mb-3">
                    <label for="lastname" class="form-label">Nom de Famille</label>
                    <input name="lastname" type="text" class="form-control" id="lastname" required>
                </div>
                <div class="mb-3">
                    <label for="firstname" class="form-label">Prenom</label>
                    <input name="firstname" type="text" class="form-control" id="firstname" required>
                </div>
                <div class="mb-3">
                    <label for="birthdate" class="form-label">Date de Naissance</label>
                    <input name="birthdate" type="date" class="form-control" id="birthdate" max="<?= date('Y-m-d') ?>" required>
                </div>
                <div class="mb-3">
                    <label for="phone" class="form-label">Numero de telephone</label>
                    <input name="phone" type="text" class="form-control" id="phone" required>
                </div>
                <div class="mb-3">
                    <label for="mail" class="form-label">Adresse Mail</label>
                    <input name="mail" type="email" class="form-control" id="mail" required>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col">

        </div>
    </div>
</div>



<?php
require_once('class/View/footer.php');
