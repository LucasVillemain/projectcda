<?php
require_once('class/View/header.php');
/**
 * @var array $listePatients
 * @var int $page
 * @var int $maxPages
 */
?>

<div class="container">
    <div>

        <a href="<?= Route::getBaseURL() ?>patient/new" class="btn btn-primary">Ajouter un nouveau patient</a>
        <div class="form-recherche">
            <form action="<?= Route::getBaseURL() . 'patient/recherche' ?>" method="post">
                <input type="text" class="form-control inline-recherche" placeholder="Chercher un patient" name="recherche">
                <button class="btn btn-outline-secondary inline-recherche" type="submit" id="button-addon2">Valider</button>
            </form>
        </div>

        <!-- pagination -->
        <div class="pagination">
            <?php
            for ($i = 1; $i <= $maxPages; $i++) {
            ?>
                <a class="btn <?= ($i !== $page) ? 'btn-info' : '' ?> element-pagination" href="<?= Route::getBaseURL() . 'patient/All/' . $i . '?recherche=' . $recherche ?? '' ?>"><?= $i ?></a>
            <?php
            }
            ?>
        </div>


    </div>
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nom</th>
                <th scope="col">Prenom</th>
                <th scope="col">Date de naissance</th>
                <th scope="col">Numero de telephone</th>
                <th scope="col">adresse mail</th>
                <th scope="col">Editer</th>
                <th scope="col">Supprimer</th>


            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($listePatients as $patient) {
            ?>
                <tr ondblclick="window.location='<?= Route::getBaseURL() . 'patient/' . $patient->id ?>'">
                    <!-- le liens renvoie vers patient/id du patient -->

                    <th scope="row"><?= $patient->id ?></th>
                    <td><?= $patient->lastname ?></td>
                    <td><?= $patient->firstname ?></td>
                    <td><?= $patient->birthdate ?></td>
                    <td><?= $patient->phone ?></td>
                    <td><?= $patient->mail ?></td>
                    <td><a href="<?= Route::getBaseURL() . 'patient/' . $patient->id ?>" class="btn btn-secondary">Afficher</a> </td>
                    <td><a href="<?= Route::getBaseURL() . 'patient/edit/' . $patient->id ?>" class="btn btn-secondary">Editer</a> </td>
                    <td><a href="<?= Route::getBaseURL() . 'patient/delete/' . $patient->id ?>" class="btn btn-secondary">Supprimer</a> </td>



                </tr>

            <?php } ?>



        </tbody>
    </table>
</div>



<?php
require_once('class/View/footer.php');
