<?php

require 'class/Autoloader.php';
Autoloader::register();
require_once('class/View/header.php');


$router = new Router($_GET['url']);

$router->get('/', "#accueil");

$router->get('/patient/:id', "Patient#show")->with('id', '[0-9]+');

$router->get('/patient/delete/:id', "Patient#delete");

$router->get('/patient/edit/:id', "Patient#loadEdit")->with('id', '[0-9]+');
$router->post('/patient/edit/:id', "Patient#edit")->with('id', '[0-9]+');

$router->get('/patient/new/appointment', "Patient#loadPlusAppointment");
$router->post('/patient/new/new', "Patient#addPlusAppointment");

$router->get('/patient/new', "Patient#add");
$router->post('/patient/new', "Patient#create");

$router->get('/patient/All/:page', "Patient#showAll")->with('page', '[0-9]+');
$router->get('/patient/All', "Patient#showAll");

$router->post('/patient/recherche', "Patient#showAll");



$router->get('/rdv/new', "Appointment#add");
$router->post('/rdv/new', "Appointment#create");

$router->get('/rdv/:id', "Appointment#show")->with('id', '[0-9]+');

$router->get('/rdv/edit/:id', "Appointment#loadEdit")->with('id', '[0-9]+');
$router->post('/rdv/edit/:id', "Appointment#edit")->with('id', '[0-9]+');

$router->get('/rdv/delete/:id', "Appointment#delete")->with('id', '[0-9]+');

$router->get('/rdv/All', "Appointment#showAll");



$router->run();


require_once('class/view/footer.php');
